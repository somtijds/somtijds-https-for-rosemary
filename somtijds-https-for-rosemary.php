<?php
/**
 * Plugin Name:     Consistent HTTPS for Rosemary
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     This plugin changes the http:// links in Rosemary to https:// for Google fonts.
 * Author:          Willem Prins | SOMTIJDS
 * Author URI:      https://www.somtijds.de
 * Text Domain:     somtijds-https-for-rosemary
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Somtijds_Https_For_Rosemary
 */

// Your code starts here.
add_action( 'wp_enqueue_scripts', 'somtijds_https_for_rosemary_de_and_reenqueue_styles', 999 );

/**
 * Enqueue scripts and styles.
 */
function somtijds_https_for_rosemary_de_and_reenqueue_styles() {

	// Fonts (remove http:// references) 
	wp_dequeue_style('default_body_font');
	wp_dequeue_style('default_heading_font');
	// Fonts (add https:// references)
	wp_enqueue_style('wp_edits_default_body_font', 'https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic&subset=latin,latin-ext');
	wp_enqueue_style('wp_edits_default_heading_font', 'https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic&subset=latin,latin-ext');
	
}